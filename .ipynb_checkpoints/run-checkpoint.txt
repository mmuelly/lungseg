docker run --name lungseg -v data_subset:/usr/deployment/data --shm-size=288g --ulimit memlock=-1 --ulimit stack=67108864 lungseg:latest

docker run --rm -it --name lungseg -v data_subset:/usr/deployment/data --shm-size=288g --ulimit memlock=-1 --ulimit stack=67108864 lungseg:latest /bin/sh

nvidia-docker run --rm -it --name lungseg -v data_subset:/usr/deployment/data_subset --gpus all --shm-size=288g --ulimit memlock=-1 --ulimit stack=67108864 lungseg:latest /bin/sh

docker run --rm -i -v=lungseg-data:/tmp/myvolume busybox find /tmp/myvolume

docker container create --name dummy -v myvolume:/root busybox
docker cp /home/jupyter/proj/lungseg/data_subset dummy:/root/