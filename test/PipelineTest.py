from src.Pipeline import PipelineTCIA


def test_extract():
    input_dimension = (64, 64, 3)
    batch_size = 16
    study_dirs = ['./Test Data/TestTCIA/']

    pipe = PipelineTCIA(input_dimension, batch_size)
    pipe.extract(study_dirs)

    assert len(pipe.slice_paths) == 430

    return


def test_load_from_study_dirs():
    input_dimension = (64, 64, 3)
    batch_size = 16
    study_dirs = ['./Test Data/TestTCIA/']

    pipe = PipelineTCIA(input_dimension, batch_size)
    pipe.load_from_study_dirs(study_dirs)

    assert len(pipe.train_slice_paths) == 256
    assert len(pipe.test_slice_paths) == 64

    return


def test_split_data():
    input_dimension = (64, 64, 3)
    batch_size = 16
    study_dirs = ['./Test Data/TestTCIA/']

    pipe = PipelineTCIA(input_dimension, batch_size)
    pipe.extract(study_dirs)

    pipe.split_data()

    assert len(pipe.train_slice_paths) == 344
    assert len(pipe.test_slice_paths) == 86

    return


def test_load_from_dicom():

    dicom_file_path = './Test Data/TestTCIA/TestSubjectLungs/01-01-2000-CT CHEST WO CON-40731' \
                      '/4.000000-Recon 3 CHEST 5X5-98181/1-001.dcm'

    dicom = PipelineTCIA.load_from_dicom(dicom_file_path)

    import numpy as np
    dicom_ground_truth = np.load('./Test Data/test_load_from_dicom.npy')

    assert np.all(np.equal(dicom, dicom_ground_truth))

    return


def test_store_slice_paths_as_csv():
    input_dimension = (64, 64, 3)
    batch_size = 16
    study_dirs = ['./Test Data/TestTCIA/']

    pipe = PipelineTCIA(input_dimension, batch_size)
    pipe.load_from_study_dirs(study_dirs)
    pipe.store_slice_paths_as_csv('./Test Data/test.csv')

    import os
    assert os.path.isfile('./Test Data/test.csv')

    return


def test_load_slice_paths_from_csv():
    input_dimension = (64, 64, 3)
    batch_size = 16
    csv_path = './Test Data/test.csv'

    pipe = PipelineTCIA(input_dimension, batch_size)
    pipe.load_slice_paths_from_csv(csv_path)

    assert len(pipe.train_slice_paths) == 256
    assert len(pipe.test_slice_paths) == 64

    return


def reduce_data_to_whole_batches_test_helper(input_dimension, batch_size):

    csv_path = './Test Data/test.csv'

    pipe = PipelineTCIA(input_dimension, batch_size)
    pipe.load_slice_paths_from_csv(csv_path)

    assert (len(pipe.test_slice_paths) % batch_size) == 0
    assert (len(pipe.test_slice_paths) % batch_size) == 0

    return


def test_reduce_data_to_whole_batches():
    input_dimension = (64, 64, 3)

    reduce_data_to_whole_batches_test_helper(input_dimension, batch_size=16)
    reduce_data_to_whole_batches_test_helper(input_dimension, batch_size=32)
    try:
        reduce_data_to_whole_batches_test_helper(input_dimension, batch_size=0)
    except AssertionError:
        pass
    else:
        raise Exception('Batch size of less than zero rejected')

    return


def test_find_slice_category_counts():

    csv_path = './Test Data/test.csv'

    import pandas as pd

    slice_path_dataframe = pd.read_csv(csv_path, header=None)
    counts = PipelineTCIA.find_slice_category_counts(slice_path_dataframe)

    lungs_slices = counts[0]
    non_lung_slices = counts[1]

    assert lungs_slices == 161 and non_lung_slices == 161

    return


def test_yield_batch():

    input_dimension = (64, 64, 3)
    batch_size = 16
    csv_path = './Test Data/test.csv'

    pipe = PipelineTCIA(input_dimension, batch_size)
    pipe.load_slice_paths_from_csv(csv_path)

    batch = next(pipe.yield_batch(batch_size))

    assert batch[0].shape == (16, 64, 64, 3)
    assert batch[1].size == 16

    return


def test_yield_single_train_data_pair():

    input_dimension = (64, 64, 3)
    batch_size = 16
    csv_path = './Test Data/test.csv'

    pipe = PipelineTCIA(input_dimension, batch_size)
    pipe.load_slice_paths_from_csv(csv_path)

    batch = next(pipe.yield_single_train_data_pair())

    assert batch[0].shape == (64, 64, 3)
    assert batch[1].size == 1

    return


def test_balance_dataset():

    input_dimension = (64, 64, 3)
    batch_size = 16
    study_dirs = ['./Test Data/TestTCIA/']

    import pandas as pd
    pipe = PipelineTCIA(input_dimension, batch_size)
    pipe.extract(study_dirs)
    slice_path_df = pd.DataFrame(pipe.slice_paths)
    pipe.counts = pipe.find_slice_category_counts(slice_path_df)

    lungs_slices = pipe.counts[0]
    non_lung_slices = pipe.counts[1]

    assert lungs_slices == 161 and non_lung_slices == 269

    pipe.balance_dataset()

    lungs_slices = pipe.counts[0]
    non_lung_slices = pipe.counts[1]

    assert lungs_slices == 161 and non_lung_slices == 161

    return


def test_load_from_study_dir_and_store():
    input_dimension = (64, 64, 3)
    batch_size = 16
    study_dirs = ['./Test Data/TestTCIA/']
    csv_path = './Test Data/test_load_and_store.csv'

    pipe = PipelineTCIA(input_dimension, batch_size)
    pipe.load_from_study_dir_and_store(study_dirs, csv_path)

    import os
    assert os.path.isfile('./Test Data/test_load_and_store.csv')

    return
