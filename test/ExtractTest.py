from src.Extract import ExtractorTCIA


def test_get_subjects():
    test_dir = './Test Data/TestTCIA/'
    subject_dirs, subjects = ExtractorTCIA.get_subjects(test_dir)

    assert sorted(subjects) == sorted(['TestSubjectExluded', 'TestSubjectHead', 'TestSubjectLungs',
                                       'TestSubjectPelvis'])

    return


def test_get_categorized_slice_paths():
    test_dir = './Test Data/TestTCIA/'
    verbosity = 0
    extractor = ExtractorTCIA(test_dir, verbosity)

    assert len(extractor.slice_paths) == 430

    return


def test_filter_dicom():

    excluded_path = './Test Data/TestTCIA/TestSubjectExluded/12-14-1998-PETCT HEADNECK CA IN-57432' \
                    '/4.000000-PET AC-04924/1-001.dcm'

    head_path = './Test Data/TestTCIA/TestSubjectHead/05-08-2001-PETCT HEAD  NECK CA-40464' \
                '/7.000000-CT Atten Cor HN-13942/1-001.dcm'

    lungs_path = './Test Data/TestTCIA/TestSubjectLungs/01-01-2000-CT CHEST WO CON-40731' \
                 '/4.000000-Recon 3 CHEST 5X5-98181/1-001.dcm'

    pelvis_path = './Test Data/TestTCIA/TestSubjectPelvis/TestVisit/12.000000-MiednicaPOZNE  5.0  B31f-37285/1-01.dcm'

    assert ExtractorTCIA.filter_dicom(excluded_path) == 'Excl'
    assert ExtractorTCIA.filter_dicom(head_path) == 'Not Lungs'
    assert ExtractorTCIA.filter_dicom(lungs_path) == 'Lungs'
    assert ExtractorTCIA.filter_dicom(pelvis_path) == 'Not Lungs'

    return
