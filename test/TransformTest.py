from src.Transform import TransformerTCIA
from src.Pipeline import PipelineTCIA


def test_transform():

    test_paths = {'./Test Data/test_transform_excl': './Test Data/TestTCIA/TestSubjectExluded'
                                                     '/12-14-1998-PETCT HEADNECK CA IN-57432'
                                                     '/4.000000-PET AC-04924/1-001.dcm',

                  './Test Data/test_transform_head': './Test Data/TestTCIA/TestSubjectHead'
                                                     '/05-08-2001-PETCT HEAD  NECK CA-40464'
                                                     '/7.000000-CT Atten Cor HN-13942/1-001.dcm',

                  './Test Data/test_transform_lungs': './Test Data/TestTCIA/TestSubjectLungs'
                                                      '/01-01-2000-CT CHEST WO CON-40731'
                                                      '/4.000000-Recon 3 CHEST 5X5-98181/1-001.dcm',

                  './Test Data/test_transform_pelvis': './Test Data/TestTCIA/TestSubjectPelvis/TestVisit'
                                                       '/12.000000-MiednicaPOZNE  5.0  B31f-37285/1-01.dcm'}

    input_dimension = (64, 64, 3)
    batch_size = 16

    pipe = PipelineTCIA(input_dimension, batch_size)

    for file, path in test_paths.items():
        dicom = pipe.load_from_dicom(path)
        transformed_dicom = TransformerTCIA.transform(dicom, input_dimension)

        import numpy as np
        dicom_ground_truth = np.load(file + '.npy')

        assert np.all(np.equal(transformed_dicom, dicom_ground_truth))

    return


def test_resize():

    test_paths = {'./Test Data/test_resize_excl': './Test Data/TestTCIA/TestSubjectExluded'
                                                  '/12-14-1998-PETCT HEADNECK CA IN-57432'
                                                  '/4.000000-PET AC-04924/1-001.dcm',

                  './Test Data/test_resize_head': './Test Data/TestTCIA/TestSubjectHead'
                                                  '/05-08-2001-PETCT HEAD  NECK CA-40464'
                                                  '/7.000000-CT Atten Cor HN-13942/1-001.dcm',

                  './Test Data/test_resize_lungs': './Test Data/TestTCIA/TestSubjectLungs'
                                                   '/01-01-2000-CT CHEST WO CON-40731'
                                                   '/4.000000-Recon 3 CHEST 5X5-98181/1-001.dcm',

                  './Test Data/test_resize_pelvis': './Test Data/TestTCIA/TestSubjectPelvis/TestVisit'
                                                    '/12.000000-MiednicaPOZNE  5.0  B31f-37285/1-01.dcm'}

    input_dimension = (64, 64, 3)
    batch_size = 16

    pipe = PipelineTCIA(input_dimension, batch_size)

    for file, path in test_paths.items():
        dicom = pipe.load_from_dicom(path)
        resized_dicom = TransformerTCIA.resize(dicom, input_dimension[1], input_dimension[0])

        import numpy as np

        dicom_ground_truth = np.load(file + '.npy')
        assert np.all(np.equal(resized_dicom, dicom_ground_truth))

    return


def test_min_max_threshold():

    test_paths = {'./Test Data/test_min_maxe_excl': './Test Data/TestTCIA/TestSubjectExluded'
                                                    '/12-14-1998-PETCT HEADNECK CA IN-57432'
                                                    '/4.000000-PET AC-04924/1-001.dcm',

                  './Test Data/test_min_max_head': './Test Data/TestTCIA/TestSubjectHead'
                                                   '/05-08-2001-PETCT HEAD  NECK CA-40464'
                                                   '/7.000000-CT Atten Cor HN-13942/1-001.dcm',

                  './Test Data/test_min_max_lungs': './Test Data/TestTCIA/TestSubjectLungs'
                                                    '/01-01-2000-CT CHEST WO CON-40731'
                                                    '/4.000000-Recon 3 CHEST 5X5-98181/1-001.dcm',

                  './Test Data/test_min_max_pelvis': './Test Data/TestTCIA/TestSubjectPelvis/TestVisit'
                                                     '/12.000000-MiednicaPOZNE  5.0  B31f-37285/1-01.dcm'}

    input_dimension = (64, 64, 3)
    batch_size = 16

    pipe = PipelineTCIA(input_dimension, batch_size)

    threshold_min_orig = -2048
    threshold_max_orig = 2048

    for file, path in test_paths.items():
        dicom = pipe.load_from_dicom(path)
        threshold_dicom = TransformerTCIA.min_max_threshold(dicom, threshold_min_orig, threshold_max_orig)

        import numpy as np

        dicom_ground_truth = np.load(file + '.npy')
        assert np.all(np.equal(threshold_dicom, dicom_ground_truth))

    return


def test_normalize():

    test_paths = {'./Test Data/test_normalize_excl': './Test Data/TestTCIA/TestSubjectExluded'
                                                     '/12-14-1998-PETCT HEADNECK CA IN-57432'
                                                     '/4.000000-PET AC-04924/1-001.dcm',

                  './Test Data/test_normalize_head': './Test Data/TestTCIA/TestSubjectHead'
                                                     '/05-08-2001-PETCT HEAD  NECK CA-40464'
                                                     '/7.000000-CT Atten Cor HN-13942/1-001.dcm',

                  './Test Data/test_normalize_lungs': './Test Data/TestTCIA/TestSubjectLungs'
                                                      '/01-01-2000-CT CHEST WO CON-40731'
                                                      '/4.000000-Recon 3 CHEST 5X5-98181/1-001.dcm',

                  './Test Data/test_normalize_pelvis': './Test Data/TestTCIA/TestSubjectPelvis/TestVisit'
                                                       '/12.000000-MiednicaPOZNE  5.0  B31f-37285/1-01.dcm'}

    input_dimension = (64, 64, 3)
    batch_size = 16

    pipe = PipelineTCIA(input_dimension, batch_size)

    threshold_min_orig = -2048
    threshold_max_orig = 2048

    for file, path in test_paths.items():
        dicom = pipe.load_from_dicom(path)
        threshold_dicom = TransformerTCIA.min_max_threshold(dicom, threshold_min_orig, threshold_max_orig)
        normalized_dicom = TransformerTCIA.normalize(threshold_dicom, threshold_min_orig, threshold_max_orig)

        import numpy as np

        dicom_ground_truth = np.load(file + '.npy')
        assert np.all(np.equal(normalized_dicom, dicom_ground_truth))

    return


def test_convert_16b_float_to_8b_int():

    test_paths = {'./Test Data/test_convert_16b_float_to_8b_int_excl': './Test Data/TestTCIA/TestSubjectExluded'
                                                                       '/12-14-1998-PETCT HEADNECK CA IN-57432'
                                                                       '/4.000000-PET AC-04924/1-001.dcm',

                  './Test Data/test_convert_16b_float_to_8b_int_head': './Test Data/TestTCIA/TestSubjectHead'
                                                                       '/05-08-2001-PETCT HEAD  NECK CA-40464'
                                                                       '/7.000000-CT Atten Cor HN-13942/1-001.dcm',

                  './Test Data/test_convert_16b_float_to_8b_int_lungs': './Test Data/TestTCIA/TestSubjectLungs'
                                                                        '/01-01-2000-CT CHEST WO CON-40731'
                                                                        '/4.000000-Recon 3 CHEST 5X5-98181/1-001.dcm',

                  './Test Data/test_convert_16b_float_to_8b_int_pelvis': './Test Data/TestTCIA/'
                                                                         'TestSubjectPelvis/TestVisit'
                                                                         '/12.000000-MiednicaPOZNE  5.0  B31f-37285'
                                                                         '/1-01.dcm'}

    input_dimension = (64, 64, 3)
    batch_size = 16

    pipe = PipelineTCIA(input_dimension, batch_size)

    for file, path in test_paths.items():
        dicom = pipe.load_from_dicom(path)
        converted_8b_dicom = TransformerTCIA.convert_16b_float_to_8b_int(dicom)

        import numpy as np

        dicom_ground_truth = np.load(file + '.npy')
        assert np.all(np.equal(converted_8b_dicom, dicom_ground_truth))

    return


def test_convert_8b_int_to_32b_float():

    test_paths = {'./Test Data/test_convert_8b_int_to_32b_float_excl': './Test Data/TestTCIA/TestSubjectExluded'
                                                                       '/12-14-1998-PETCT HEADNECK CA IN-57432'
                                                                       '/4.000000-PET AC-04924/1-001.dcm',

                  './Test Data/test_convert_8b_int_to_32b_float_head': './Test Data/TestTCIA/TestSubjectHead'
                                                                       '/05-08-2001-PETCT HEAD  NECK CA-40464'
                                                                       '/7.000000-CT Atten Cor HN-13942/1-001.dcm',

                  './Test Data/test_convert_8b_int_to_32b_float_lungs': './Test Data/TestTCIA/TestSubjectLungs'
                                                                        '/01-01-2000-CT CHEST WO CON-40731'
                                                                        '/4.000000-Recon 3 CHEST 5X5-98181/1-001.dcm',

                  './Test Data/test_convert_8b_int_to_32b_float_pelvis': './Test Data/TestTCIA/TestSubjectPelvis'
                                                                         '/TestVisit'
                                                                         '/12.000000-MiednicaPOZNE  5.0  B31f-37285'
                                                                         '/1-01.dcm'}

    input_dimension = (64, 64, 3)
    batch_size = 16

    pipe = PipelineTCIA(input_dimension, batch_size)

    for file, path in test_paths.items():
        dicom = pipe.load_from_dicom(path)
        converted_8b_dicom = TransformerTCIA.convert_16b_float_to_8b_int(dicom)
        converted_32b_dicom = TransformerTCIA.convert_8b_int_to_32b_float(converted_8b_dicom)

        import numpy as np
        dicom_ground_truth = np.load(file + '.npy')

        assert np.all(np.equal(converted_32b_dicom, dicom_ground_truth))

    return


def test_otsu_threshold():

    test_paths = {'./Test Data/test_otsu_threshold_excl': './Test Data/TestTCIA/TestSubjectExluded'
                                                          '/12-14-1998-PETCT HEADNECK CA IN-57432/'
                                                          '4.000000-PET AC-04924/1-001.dcm',

                  './Test Data/test_otsu_threshold_head': './Test Data/TestTCIA/TestSubjectHead'
                                                          '/05-08-2001-PETCT HEAD  NECK CA-40464'
                                                          '/7.000000-CT Atten Cor HN-13942/1-001.dcm',

                  './Test Data/test_otsu_threshold_lungs': './Test Data/TestTCIA/TestSubjectLungs'
                                                           '/01-01-2000-CT CHEST WO CON-40731'
                                                           '/4.000000-Recon 3 CHEST 5X5-98181/1-001.dcm',

                  './Test Data/test_otsu_threshold_pelvis': './Test Data/TestTCIA/TestSubjectPelvis/TestVisit'
                                                            '/12.000000-MiednicaPOZNE  5.0  B31f-37285/1-01.dcm'}

    input_dimension = (64, 64, 3)
    batch_size = 16

    pipe = PipelineTCIA(input_dimension, batch_size)

    for file, path in test_paths.items():
        dicom = pipe.load_from_dicom(path)
        converted_8b_array = TransformerTCIA.convert_16b_float_to_8b_int(dicom)
        otsu_array = TransformerTCIA.otsu_threshold(converted_8b_array)

        import numpy as np
        dicom_ground_truth = np.load(file + '.npy')

        assert np.all(np.equal(otsu_array, dicom_ground_truth))

    return


def test_adaptive_threshold():

    test_paths = {'./Test Data/test_adaptive_threshold_excl': './Test Data/TestTCIA/TestSubjectExluded'
                                                              '/12-14-1998-PETCT HEADNECK CA IN-57432'
                                                              '/4.000000-PET AC-04924/1-001.dcm',

                  './Test Data/test_adaptive_threshold_head': './Test Data/TestTCIA/TestSubjectHead'
                                                              '/05-08-2001-PETCT HEAD  NECK CA-40464'
                                                              '/7.000000-CT Atten Cor HN-13942/1-001.dcm',

                  './Test Data/test_adaptive_threshold_lungs': './Test Data/TestTCIA/TestSubjectLungs'
                                                               '/01-01-2000-CT CHEST WO CON-40731'
                                                               '/4.000000-Recon 3 CHEST 5X5-98181/1-001.dcm',

                  './Test Data/test_adaptive_threshold_pelvis': './Test Data/TestTCIA/TestSubjectPelvis/TestVisit'
                                                                '/12.000000-MiednicaPOZNE  5.0  B31f-37285/1-01.dcm'}

    input_dimension = (64, 64, 3)
    batch_size = 16

    pipe = PipelineTCIA(input_dimension, batch_size)

    for file, path in test_paths.items():
        dicom = pipe.load_from_dicom(path)
        converted_8b_array = TransformerTCIA.convert_16b_float_to_8b_int(dicom)
        adaptive_threshold_array = TransformerTCIA.adaptive_threshold(converted_8b_array)

        import numpy as np
        dicom_ground_truth = np.load(file + '.npy')

        assert np.all(np.equal(adaptive_threshold_array, dicom_ground_truth))

    return
