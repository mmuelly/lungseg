#lungseg

-----

## Running

1) Create runtime based off of pip requirements.txt or Anaconda environment.yml;
the original runtime was Windows 10 Home Edition build v2004
2) Download LUNA dataset, place raw data in '../data_subset/LUNA/raw_data/' and
segmentation masks in '../data_subset/LUNA/segmentation_masks/'
3) Execute program

## Overview

This is a machine learning project backed by Imvaria designed to do binary classification of 
lung/not lung 'slices' within CT scans. Environment requirements are in requirements.txt.

## Data
The data is pulled from The Cancer Imaging Archive (TCIA) slices with slices from the 
Lung Imaging Database Consortium (LIDC) classified as lung slices. 

Slices from the Head and Neck Squamous Cell Carcinoma (HNSCC) and 
Clinical Proteomic Tumor Analysis Consortium Uterine Corpus Endometrial Carcinoma (CPTAC-UCEC) datasets 
are classified as non-lung as long as they have the proper modality and are in singificantly different 
sections in the body from the Lungs.

## Model

The model is currently rudementary, and is built in Tensorflow. The data and input to the 
model is downsampled by x64 for initial framework spin-up.

## Results

The model currently polls the following metrics:

Receiver Operator Characteristic AUC: ~99% \
Performance Recall Curve AUC ~99% \
Confidence Matrix:\
Row 0: Not Lungs\
Row 1: Lungs\
Column 0: Not Lungs\
Column 1: Lungs

| 473 | 1   |
|-----|-----|
| 42  | 412 |

Overall Accuracy: 95.366%

# Per-File Description

## Augment.py

     * Augmentor:
          The augmentor class is responsible for all data augmentation within the pipeline.
          It has options for adding salt and pepper noise as well as the various translations 
          and transformations provided by tensorflow.keras.preprocessing.image.ImageDataGenerator.

## Evaluate.py

     * Evaluator:
          Heavily leverages sklearn.metrics library to evaluate the performance of a model from 
          the calculated predictions of slices versus their ground truths. 
          
## Extract.py

    * ExtractorTCIA:
          Used for retrieving the CT slice paths from TCIA datasets, but this class is not used if 
          the CT slice paths have already been saved to a csv. Saving the slice paths to a csv is 
          much faster, and is recommended after the data has initially been extracted.

    * ExtractorLUNA
          Used for retrieving CT slice paths from the LUNA dataset.
  
## Main.py

     Main.py houses the most top-level syntax for creating the pipeline, creating and training 
     a model, and evaluating the model off of its predictions.

## Model.py


    * ModelCNNV1:
          Houses the first version of the Tensorflow model used for binary classification

## Pipeline.py

    
    * PipelineTCIA:
          Creates the most top-level framework for taking the data from either the ExtractorTCIA 
          or the pre-compiled CSV, transforming it into the necessary format, augmenting it 
          if necessary, and loading it into the model for training or validation.
         
    * PipelineLUNA:
          Creates the most top-level framework for the LUNA dataset's data pipeline, 
          used for the classical Contour Tracing Correction model.

## Sand.py

     A sandbox file for data exploration / experimental development

## Task.py

     * TaskerTCIA:
          Recieves a model load path if applicable, creates the model, and then either 
          trains the model on a preconfigure pipeline or tests it against a preconfigured pipeline.

## Transform.py

     * TransformerTCIA:
          Takes the original 512x512 slice and transforms it to the dimensions required by the model. 
          Also appends the data after Otsu Thresholding and Adaptive Thresholding channels to the inpit.

## Visualize.py

      * Visualizer:
          In charge of all visualization needed for the duration of development. 
          Currently mainly visualizes data and the evaluative metrics.










