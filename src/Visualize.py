from src.Pipeline import PipelineTCIA
from src.Evaluate import Evaluator
from tensorflow.keras.callbacks import History
import matplotlib.pyplot as plt
import seaborn as sn
import pandas as pd
import numpy as np
from skimage import measure

plt.ioff()


class Visualizer:
    @staticmethod
    def plot_image(numpy_array: np.ndarray):
        """
        :param numpy_array: 2D Numpy array to display
        :return None:
        """

        plt.imshow(numpy_array, cmap='Greys')
        plt.show()
        plt.clf()
        return

    @staticmethod
    def visualize_volume(volume: np.ndarray):
        """
        :param volume: 3DNumpy array to display
        :return None:
        """

        volume_np = np.asarray(volume)
        volume_np = np.moveaxis(volume_np, 0, -1)

        verts, faces, normals, values = measure.marching_cubes(volume_np, 0, spacing=(1, 1, 1))

        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')
        ax.plot_trisurf(verts[:, 0], verts[:, 1], faces, verts[:, 2],
                        cmap='Spectral', lw=1)
        plt.show()

        return

    @staticmethod
    def visualize_dataset(store_path_: str):
        """
        :param store_path_: String which contains a path to a csv which contains the dataset paths.
        :return None:
        """

        dimensions_ = (64, 64, 3)
        batch_size_ = 16

        pipe_ = PipelineTCIA(dimensions_, batch_size_)
        pipe_.load_slice_paths_from_csv(store_path_)

        while True:
            temp = next(pipe_.yield_single_train_data_pair(mode='Test'))

            print(temp[1])
            Visualizer.plot_image(temp[0][:, :, 0])
            Visualizer.plot_image(temp[0][:, :, 1])
            Visualizer.plot_image(temp[0][:, :, 2])

        return

    @staticmethod
    def visualize_metrics(ground_truths: list, predictions: list, view: bool = False, save_dir: str = None):
        """
        :param ground_truths: A list of the ground truths corresponding to the input that the 
                              model has previously been predicted upon.
        :param predictions: A list of the predictions that the model has generated from a list of
                            inputs. Must match the ground truths in length and respective original 
                            image.
        :param view: Whether the resulting metrics should yield a Matplotlib popup figure
        :param save_dir: What directory, if any, the resulting metrics should be saved to.
        :return None: 
        """

        Visualizer.visualize_receiver_operating_characteristic_curve(ground_truths, predictions, view, save_dir)
        Visualizer.visualize_precision_recall_curve(ground_truths, predictions, view, save_dir)
        Visualizer.visualize_confusion_matrix(ground_truths, predictions, view, save_dir)

        accuracy = Evaluator.generate_accuracy(ground_truths, predictions)
        print('Accuracy: ' + str(accuracy))

        return

    @staticmethod
    def visualize_receiver_operating_characteristic_curve(ground_truths: list, predictions: list, view: bool = False,
                                                         save_dir: str = None):
        """
        :param ground_truths: A list of the ground truths corresponding to the input that the 
                              model has previously been predicted upon.
        :param predictions: A list of the predictions that the model has generated from a list of
                            inputs. Must match the ground truths in length and respective original 
                            image.
        :param view: Whether the resulting receiver operating characteristic _curve should yield a Matplotlib popup figure
        :param save_dir: What directory, if any, the resulting receiver operating characteristic curve should be saved to.
        :return None: 
        """
        
        false_positive_rate, true_positive_rate, thresholds, roc_auc = Evaluator.\
            generate_receiver_operating_characteristic_curve(ground_truths, predictions)

        plt.title('Receiver Operating Characteristic')
        plt.plot(false_positive_rate, true_positive_rate, 'b', label='AUC = %0.2f' % roc_auc)
        plt.legend(loc='lower right')
        plt.plot([0, 1], [0, 1], 'r--')
        plt.xlim([0, 1])
        plt.ylim([0, 1])
        plt.ylabel('True Positive Rate')
        plt.xlabel('False Positive Rate')

        if save_dir is not None:
            save_dir = save_dir + str('Receiver Operator Characteristic Curve.jpg')

        Visualizer.control_pyplot_completion(view, save_dir)

        return

    @staticmethod
    def visualize_precision_recall_curve(ground_truths: list, predictions: list, view: bool = False,
                                         save_dir: str = None):
        """
        :param ground_truths: A list of the ground truths corresponding to the input that the 
                              model has previously been predicted upon.
        :param predictions: A list of the predictions that the model has generated from a list of
                            inputs. Must match the ground truths in length and respective original 
                            image.
        :param view: Whether the resulting precision recall curve should yield a Matplotlib popup figure
        :param save_dir: What directory, if any, the resulting precision recall curve should be saved to.
        :return None: 
        """
        
        precision, recall, thresholds, prc_auc = Evaluator.\
            generate_precision_recall_curve(ground_truths, predictions)

        plt.title('Precision Recall')
        plt.plot(recall, precision, 'b', label='AUC = %0.2f' % prc_auc)
        plt.legend(loc='lower right')
        plt.plot([0, 1], [0, 1], 'r--')
        plt.xlim([0, 1])
        plt.ylim([0, 1])
        plt.ylabel('Precision')
        plt.xlabel('Recall')

        if save_dir is not None:
            save_dir = save_dir + str('Precision Recall Curve.jpg')

        Visualizer.control_pyplot_completion(view, save_dir)

        return

    @staticmethod
    def visualize_confusion_matrix(ground_truths: list, predictions: list, view: bool = False,
                                   save_dir: str = None):
        """
        :param ground_truths: A list of the ground truths corresponding to the input that the 
                              model has previously been predicted upon.
        :param predictions: A list of the predictions that the model has generated from a list of
                            inputs. Must match the ground truths in length and respective original 
                            image.
        :param view: Whether the resulting confusion matrix should yield a Matplotlib popup figure
        :param save_dir: What directory, if any, the resulting confusion matrix should be saved to.
        :return None: 
        """
        
        confusion_matrix = Evaluator.generate_confusion_matrix(ground_truths, predictions)

        confusion_matrix_dataframe = pd.DataFrame(confusion_matrix)
        ax = sn.heatmap(confusion_matrix_dataframe, annot=True, annot_kws={"size": 16}, fmt='g',
                        xticklabels=('Not Lungs', 'Lungs'),
                        yticklabels=('Not Lungs', 'Lungs'))

        ax.set(xlabel='Predicted', ylabel='Actual')
        ax.set_title('Confusion Matrix')

        if save_dir is not None:
            save_dir = save_dir + str('Confusion Matrix.jpg')

        Visualizer.control_pyplot_completion(view, save_dir)

        return

    @staticmethod
    def visualize_training_history(history: History, view: bool = False, save_dir: str = None):

        """
        :history: Tensorflow training accuracy and validation accuracy benchmarks 
                  during training for a given model.
        :param view: Whether the resulting history plot should yield a Matplotlib popup figure
        :param save_dir: What directory, if any, the resulting history plot should be saved to.
        :return None: 
        """

        plt.plot(history.history['accuracy'])
        plt.plot(history.history['val_accuracy'])
        plt.title('model accuracy')
        plt.ylabel('accuracy')
        plt.xlabel('epoch')
        plt.legend(['train', 'test'], loc='upper left')

        if save_dir is not None:
            save_dir = save_dir + str('Training History.jpg')

        Visualizer.control_pyplot_completion(view, save_dir)

        pass

    @staticmethod
    def control_pyplot_completion(view: bool = False, save_file_name: str = None):
        """
        Note: Handles the exit sequences for various visualization methods.
        :param view: Whether the current Matplotlib.pyplot should yield a Matplotlib popup figure
        :param save_dir: What directory, if any, the Matplotlib.pyplot should be saved to.
        :return None: 
        """
        if view:
            plt.show()
        if save_file_name is not None:
            plt.savefig(save_file_name)

        plt.clf()
        return
