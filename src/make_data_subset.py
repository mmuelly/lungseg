"""
### ONE-OFF SCRIPT, designed to create a subset of the training data based off a 
### given data paths csv.
"""


import os
import pandas as pd
from shutil import copyfile

path_to_csv = "data_paths.csv"
new_csv_save_path = "data_paths_subset.csv"

path_pd = pd.read_csv(path_to_csv, header=None)

for index, row in path_pd.iterrows():

    old_path = row[0]
    split_path = old_path.split('/')

    split_path[1] = 'data_subset'

    new_directory = split_path[0]
    for dir_index in range(1, len(split_path) - 1):
        new_directory = new_directory + '/' + split_path[dir_index]

    new_path = new_directory + '/' + split_path[-1]

    if not os.path.isdir(new_directory):
        os.makedirs(new_directory)

    copyfile(old_path, new_path)

    path_pd.at[index, 0] = new_path

path_pd.to_csv(new_csv_save_path, header=False, index=False)



