import numpy as np
from skimage.transform import resize
import cv2


class TransformerTCIA:

    @staticmethod
    def transform(numpy_array: np.ndarray, input_dimensions: tuple):
        """
        :param numpy_array: Original CT slice retrieved from a DICOM file. Has HWC dimensions of 512x512x3
        :param input_dimensions: Target dimension to transform the CT slice to, after transformation.
            Must match the dimensions of the input of a given model.
        :return multi_channel_output: np.ndarray: Numpy array in the desired dimensions to be fed into a model.
            Currently stacks the resized image, resized otsu thresholding of the image,
            and resized adaptive threshold of the image as three feature planes.
        """

        desired_h = input_dimensions[0]
        desired_w = input_dimensions[1]

        threshold_min_orig = -2048
        threshold_max_orig = 2048

        threshold_array_orig = TransformerTCIA.min_max_threshold(numpy_array, min_hu_value=threshold_min_orig,
                                                                 max_hu_value=threshold_max_orig)
        normalize_array_orig = TransformerTCIA.normalize(threshold_array_orig, min_=threshold_min_orig,
                                                         max_=threshold_max_orig)

        threshold_min = 0
        threshold_max = 400

        threshold_array = TransformerTCIA.min_max_threshold(numpy_array, min_hu_value=threshold_min,
                                                            max_hu_value=threshold_max)
        normalize_array = TransformerTCIA.normalize(threshold_array, min_=threshold_min, max_=threshold_max)

        eight_bit_array = TransformerTCIA.convert_16b_float_to_8b_int(normalize_array)

        otsu_threshold_array = TransformerTCIA.otsu_threshold(eight_bit_array)
        adaptive_threshold_array = TransformerTCIA.adaptive_threshold(eight_bit_array)

        blurred_otsu_array = cv2.GaussianBlur(otsu_threshold_array, ksize=(5, 5), sigmaX=0)
        blurred_adaptive_array = cv2.GaussianBlur(adaptive_threshold_array, ksize=(5, 5), sigmaX=0)

        final_otsu_result = TransformerTCIA.convert_8b_int_to_32b_float(blurred_otsu_array)
        final_adaptive_result = TransformerTCIA.convert_8b_int_to_32b_float(blurred_adaptive_array)

        normalize_array_orig = TransformerTCIA.resize(normalize_array_orig, desired_w, desired_h)
        final_otsu_result = TransformerTCIA.resize(final_otsu_result, desired_w, desired_h)
        final_adaptive_result = TransformerTCIA.resize(final_adaptive_result, desired_w, desired_h)

        multi_channel_output = np.stack((normalize_array_orig, final_otsu_result, final_adaptive_result), axis=-1)

        return multi_channel_output

    @staticmethod
    def resize(numpy_array: np.ndarray, desired_x: int, desired_y: int):
        """
        :param numpy_array: Numpy array to be resized
        :param desired_x: Desired X dimension to which the numpy array is scaled to
        :param desired_y: Desired X dimension to which the numpy array is scaled to
        :return resized_array: np.ndarray: numpy array resized to the desired dimensions
        """

        resized_array = resize(numpy_array, (desired_y, desired_x), anti_aliasing=True)
        return resized_array

    @staticmethod
    def min_max_threshold(numpy_array: np.ndarray, min_hu_value: float, max_hu_value: float):
        """
        :param numpy_array: Numpy array to be thresholded
        :param min_hu_value: Minimum threshold value desired in the array
        :param max_hu_value: Maximum threshold value desired in the array
        :return thresholded_array: np.ndarray: The numpy array after min/max thresholding is applied
        """

        lower_threshold_indices = numpy_array < min_hu_value
        upper_threshold_indices = numpy_array > max_hu_value

        thresholded_array = np.copy(numpy_array)
        thresholded_array[lower_threshold_indices] = min_hu_value
        thresholded_array[upper_threshold_indices] = max_hu_value

        return thresholded_array

    @staticmethod
    def normalize(numpy_array: np.ndarray, min_: float, max_: float):
        """
        :param numpy_array: Numpy array to be normalized
        :param min_ : Minimum value desired for linear normalization
        :param max_: Maximum value desired for linear normalization
        :return normalized_array: np.ndarray: The numpy array after linear normalization is applied
        """

        normalized_array = (numpy_array - min_)/(max_ - min_)
        return normalized_array

    @staticmethod
    def convert_16b_float_to_8b_int(numpy_array: np.ndarray):
        """
        :param numpy_array: Numpy array to be converted from 16 bit float to 8 bit integer.
        :return scaled_8b_array: np.ndarray: Numpy array after being linearly scaled to 8 bit integers and typecast

        Note: 8b int numpy arrays are necessary for otsu and adaptive thresholding.
        """

        scaled_array = numpy_array * 255
        scaled_8b_array = scaled_array.astype(np.uint8)
        return scaled_8b_array

    @staticmethod
    def convert_8b_int_to_32b_float(numpy_array: np.ndarray):
        """
        :param numpy_array: Numpy array to be converted from 8 bit integer to 16 bit float.
        :return  scaled_32b_array: np.ndarray: Numpy array after being linearly scaled to 32 bit floats and typecast

        Note: Normalized 32b float numpy arrays are preferred for machine learning models.
        """

        scaled_array = numpy_array.astype(np.float32)
        scaled_32b_array = scaled_array / 255
        return scaled_32b_array

    @staticmethod
    def otsu_threshold(numpy_array: np.ndarray):
        """
        :param numpy_array: Numpy array to be thresholded
        :return otsu_array: np.ndarray: Numpy array after otsu's thresholding is applied
        """

        blurred_array = cv2.GaussianBlur(numpy_array, ksize=(5, 5), sigmaX=0)
        otsu_threshold, otsu_array = cv2.threshold(blurred_array, 0, 255, cv2.THRESH_BINARY+cv2.THRESH_OTSU)
        return otsu_array

    @staticmethod
    def adaptive_threshold(numpy_array: np.ndarray):
        """
        :param numpy_array: Numpy array to be thresholded
        :return adap_threshold: np.ndarray: Numpy array after adaptive thresholding is applied
        """

        adap_threshold = cv2.adaptiveThreshold(src=numpy_array, maxValue=255,
                                               adaptiveMethod=cv2.ADAPTIVE_THRESH_GAUSSIAN_C,
                                               thresholdType=cv2.THRESH_BINARY,
                                               blockSize=13, C=0)
        return adap_threshold
