import tensorflow.keras as k
import numpy as np
import cv2
import math
import imutils
from scipy import ndimage
from collections import OrderedDict


class ModelCNNV1:
    @staticmethod
    def return_model(input_dimensions: tuple):
        """
        :param input_dimensions: The HWC dimensions that the model will accept as input
        :return _model: k.Model: Keras classifier neural network
        """

        _in = k.Input(shape=input_dimensions)
        _x = k.layers.SeparableConv2D(filters=16, kernel_size=(5, 5))(_in)
        _x = k.layers.BatchNormalization()(_x)
        _x = k.layers.Dropout(rate=0.5)(_x)

        _x = k.layers.SeparableConv2D(filters=32, kernel_size=(5, 5))(_x)
        _x = k.layers.BatchNormalization()(_x)
        _x = k.layers.Dropout(rate=0.5)(_x)

        _x = k.layers.SeparableConv2D(filters=64, kernel_size=(5, 5))(_x)
        _x = k.layers.BatchNormalization()(_x)
        _x = k.layers.Dropout(rate=0.5)(_x)

        _x = k.layers.Flatten()(_x)
        _x = k.layers.Dense(units=128)(_x)
        _x = k.layers.Dropout(rate=0.5)(_x)
        _out = k.layers.Dense(units=1, activation='sigmoid')(_x)

        _model = k.Model(inputs=_in, outputs=_out)
        _model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])

        return _model


class ModelCTCV1:
    # CTC = Contour Tracing Correction from https://www.hindawi.com/journals/cmmm/2016/2962047/

    def __call__(self, _in, *args, **kwargs):

        # Find the skin contour
        boundary, theta = ModelCTCV1.skin_boundary_detection(_in)
        # Find the lung masks
        mask, binary_mask = ModelCTCV1.contour_segmentation(boundary)
        # Refine lung masks
        binary_mask = ModelCTCV1.median_border_smoothing(binary_mask)
        # Correct lung masks to original coordinate system
        _out = ModelCTCV1.translate_mask_to_original_coordinates(binary_mask, theta, original_shape=_in.shape)

        return _out

    @staticmethod
    def translate_mask_to_original_coordinates(_x: np.ndarray, theta: float, original_shape: tuple):
        """
        :param _x: Final binary mask created within the classical model
        :param theta: Angle in degrees the image was initially rotated during Principle Component Based Image Alignment
        :param original_shape: Original shape of CT Slice (Typically (512, 512))
        :return x_cropped: np.ndarray: Final binary mask rotated and cropped to align with initial input image
        """
        # Rotate image back to its original orientation
        x_rotated = ndimage.rotate(_x, -theta, mode='constant', cval=0)

        # Crop image back to its original dimensions - rotation can add extra information on the edges
        x_rotated_rows = x_rotated.shape[0]
        x_rotated_columns = x_rotated.shape[1]
        desired_rows = original_shape[0]
        desired_columns = original_shape[1]

        delta_rows = int((x_rotated_rows - desired_rows)/2)
        delta_columns = int((x_rotated_columns - desired_columns)/2)

        x_cropped = x_rotated[delta_rows: desired_rows + delta_rows, delta_columns: desired_columns + delta_columns]

        return x_cropped

    @staticmethod
    def skin_boundary_detection(_x: np.ndarray):
        """
        Steps (1:3)/6 in the classical model

        :param _x: Raw CT horizontal slice, typically of dimensions 512x512
        :return (_x, theta): (np.ndarray, float): Tuple of the rotationally centered slice with area outside the body
            set to 0, as well as the angle that was used to rotate the slice

        Note: Reference section 2.1 of https://www.hindawi.com/journals/cmmm/2016/2962047/
            Rotates the chest slice so that the lungs are centered and selects the area containing the body
        """

        _x, theta = ModelCTCV1.principal_component_based_aligning(_x)
        mask = ModelCTCV1.mathematical_morphology_based_denoising(_x)
        _x = ModelCTCV1.connected_region_based_chest_masking(_x, mask)

        return _x, theta

    @staticmethod
    def principal_component_based_aligning(_x: np.ndarray):
        """
        Step 1/6 in the classical model

        :param _x: Raw CT horizontal slice, typically of dimensions 512x512
        :return (x_rotated, true_theta): (np.ndarray, float): Tuple of the rotationally centered slice as
            well as the angle that was used to rotate the slice

        Note: Reference section 2.1.1 of https://www.hindawi.com/journals/cmmm/2016/2962047/
            Also reference: https://www.researchgate.net/publication/
            329099555_Automatic_Image_Alignment_Using_Principal_Component_Analysis

            Performs principle component-based aligning on the bone within the given CT slice, rotationally centering
            the slice features including the lungs
        """

        # Shallow copy _x
        x_copy = np.copy(_x)

        # Extract bone
        x_copy[x_copy < 300] = 0

        # Convert to 1-channel image format
        x_copy = (x_copy / np.amax(_x)) * 255
        x_copy = x_copy.astype('uint8')

        # Otsu's threshold
        x_copy_blurred = cv2.GaussianBlur(x_copy, (5, 5), 0)
        threshold_points, x_copy_binarized = cv2.threshold(x_copy_blurred, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)

        # Begin PCA
        # Find coordinates of thresholded pixels
        x_copy_binarized_coordinates = []
        for column in range(x_copy_binarized.shape[0]):
            for row in range(x_copy_binarized.shape[1]):
                if x_copy_binarized[row, column] == 255:
                    x_copy_binarized_coordinates.append((column, row))

        x_copy_binarized_coordinates = np.asarray(x_copy_binarized_coordinates)

        # Find the covariance matrix of the thresholded coordinates
        coordinate_covariance_matrix = np.cov(x_copy_binarized_coordinates, rowvar=False)

        # Get eigenvectors of covariance matrix
        coordinate_eigenvalues, coordinate_eigenvectors = np.linalg.eig(coordinate_covariance_matrix)

        # Extract theta
        theta = math.degrees(math.acos((coordinate_eigenvectors[0][0] + coordinate_eigenvectors[1][1]) / 2))

        # Correct for arccosine range and angle
        if theta > 90:
            theta = 180 - theta

        true_theta = math.copysign(1, coordinate_eigenvectors[0][0] * coordinate_eigenvectors[1][0]) * theta

        # rotate image
        fill_value = -1000
        x_rotated = ndimage.rotate(_x, true_theta, mode='constant', cval=fill_value)

        return x_rotated, true_theta

    @staticmethod
    def mathematical_morphology_based_denoising(_x: np.ndarray):
        """
        Step 2/6 in the classical model

        :param _x: Raw rotationally centered CT horizontal slice
        :return x_thresholded_opened: np.ndarray: _x binarized into a mask, where dense pixels in the body are 1 and
            pixels outside the body are 0

        Note: Reference section 2.1.2 of https://www.hindawi.com/journals/cmmm/2016/2962047/

            Performs initial binarization of the slice, selecting the chest/body
        """

        # Shallow copy _x
        x_copy = np.copy(_x)

        # Initial thresholding
        lower_threshold = -1000
        x_copy[x_copy < lower_threshold] = lower_threshold
        x_copy -= lower_threshold
        x_copy = (x_copy / np.amax(_x)) * 255
        x_copy = x_copy.astype('uint8')

        # Otsu thresholding
        x_gaussian_blurred = cv2.GaussianBlur(x_copy, (7, 7), 0)
        threshold_points, x_otsu_thresholded = cv2.threshold(x_gaussian_blurred, 0, 255,
                                                             cv2.THRESH_BINARY + cv2.THRESH_OTSU)

        # Image opening
        kernel = np.ones((7, 7), np.uint8)
        x_thresholded_opened = cv2.morphologyEx(x_otsu_thresholded, cv2.MORPH_OPEN, kernel)

        # Identify islands of bone in image
        x_copy_islands, number_of_islands = ndimage.measurements.label(x_thresholded_opened)
        label_size = [(x_copy_islands == label).sum() for label in range(number_of_islands + 1)]

        # Remove islands less than 100 pixels large
        for label, size in enumerate(label_size):
            if size < 96:
                x_thresholded_opened[x_copy_islands == label] = 0

        return x_thresholded_opened

    @staticmethod
    def connected_region_based_chest_masking(_x: np.ndarray, mask: np.ndarray):
        """
        Step 3/6 in the classical model

        :param _x: Rotationally aligned raw lung slice
        :param mask: Initial binary mask of _x
        :return x_copy: np.ndarray: _x with pixels outside of the connected region of the filled / expanded
            mask set to 0

        Note: Reference section 2.1.2 of https://www.hindawi.com/journals/cmmm/2016/2962047/
                Although not explicitly described, this is the "connected regional analysis" in section 2.1.2

        """
        # Shallow copy _x, mask
        x_copy = np.copy(_x)
        mask_copy = np.copy(mask)

        # Find the largest contour within the mask
        contours = cv2.findContours(mask_copy, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        contours = imutils.grab_contours(contours)
        sorted_contours = list(reversed(sorted(contours, key=cv2.contourArea)))
        max_contour = sorted_contours[0]

        # Fill the largest contour as best as possible
        blank_image = np.zeros_like(mask_copy)
        contoured_image = cv2.fillConvexPoly(blank_image, max_contour, 255)

        # Contour not always completely enclosed - directional pooling makes up the difference
        directionally_pooled_images = ModelCTCV1.directional_pooling(contoured_image)

        # Set pixels outside the body to 0
        x_copy[directionally_pooled_images == 0] = 0

        return x_copy

    @staticmethod
    def contour_segmentation(_x: np.ndarray):
        """
        Steps (4:5)/6 in the classical model
        :param _x: Rotationally aligned CT slice with pixels outside of the skin boundary set to 0
        :return (_x, _x_mask): tuple of _x with pixels outside of the lung boundaries set to 0, as well as the
            corresponding binary mask.

        Note: Reference section 2.2 of https://www.hindawi.com/journals/cmmm/2016/2962047/

            Finds a mask of the lungs for the corresponding rotationally centered input CT slice
        """
        _x_mask, _x_boundary = ModelCTCV1.diagonal_tracing_based_lung_contour_init(_x)
        _x, _x_mask = ModelCTCV1.maximum_cost_path_based_lung_separation(_x, _x_mask)

        return _x, _x_mask

    @staticmethod
    def diagonal_tracing_based_lung_contour_init(_x: np.ndarray):
        """
        Step 4/6 in the classical model

        :param _x: Rotationally aligned CT slice with pixels outside of the skin boundary set to 0
        :return (binary_mask, boundary_mask): (np.ndarray, np.ndarray): Tuple of the mask of lung pixels as well as the
            mask's contour

        Note: Reference section 2.2.1 of https://www.hindawi.com/journals/cmmm/2016/2962047/

            Scans the two diagonals within the image for pixels that are within the lungs. Once found, Moore's
                Neighborhood boundary tracing algorithm is used to find the contour, which is then filled.
        """
        # Shallow copy _x
        x_copy = np.copy(_x)

        # Scan image for the constant number of contiguous pixels below the given threshold
        threshold = -350
        number_contiguous_thresholds = 3
        diagonal_column = number_contiguous_thresholds - 1

        # Begin scan for left lung, from the lower left pixel to the upper right pixel
        left_lung_row = None
        left_lung_column = None

        for col_iter in range(diagonal_column, x_copy.shape[1]):
            threshold_bool_ar = []
            current_row = None
            current_column = None

            for diagonal_iter in range(number_contiguous_thresholds):
                current_column = col_iter - diagonal_iter
                current_row = x_copy.shape[0] - current_column - 1
                threshold_bool_ar.append(x_copy[current_row, current_column] < threshold)

            if all(threshold_bool_ar):
                left_lung_row = current_row
                left_lung_column = current_column
                break

        # Begin scan for right lung, from the lower right to the upper left pixel
        right_lung_row = None
        right_lung_column = None

        for col_iter in range(x_copy.shape[1] - 1, diagonal_column - 1, -1):
            threshold_bool_ar = []
            current_row = None
            current_column = None

            for diagonal_iter in range(number_contiguous_thresholds):
                current_column = col_iter - diagonal_iter
                current_row = current_column
                threshold_bool_ar.append(x_copy[current_row, current_column] < threshold)

            if all(threshold_bool_ar):
                right_lung_row = current_row + number_contiguous_thresholds
                right_lung_column = current_column + number_contiguous_thresholds
                break

        # Binarize raw image based off the given threshold
        x_copy[x_copy >= threshold] = 1
        x_copy[x_copy < threshold] = 0

        # Instantiate left and right lung masks
        left_lung_boundary_mask = np.zeros_like(x_copy)
        right_lung_boundary_mask = np.zeros_like(x_copy)

        # If notable starting pixels were found within the left lung diagonal...
        if left_lung_row is not None and left_lung_column is not None:

            # Find the boundary of the region of interest that includes the notable starting pixels
            left_lung_starting_direction = 7
            left_lung_boundary = ModelCTCV1.moore_neighborhood_boundary_tracing((left_lung_column, left_lung_row),
                                                                                x_copy,
                                                                                left_lung_starting_direction)
            # Draw the left lung contour
            for position in left_lung_boundary:
                left_lung_boundary_mask[position[1], position[0]] = 255

        # If notable starting pixels were found within the right lung diagonal...
        if right_lung_row is not None and right_lung_column is not None:

            # Find the boundary of the region of interest that includes the notable starting pixels
            right_lung_starting_direction = 5
            right_lung_boundary = ModelCTCV1.moore_neighborhood_boundary_tracing((right_lung_column, right_lung_row),
                                                                                 x_copy,
                                                                                 right_lung_starting_direction)

            # Draw the right lung contour
            for position in right_lung_boundary:
                right_lung_boundary_mask[position[1], position[0]] = 255

        # Add the two masks together
        boundary_mask = left_lung_boundary_mask + right_lung_boundary_mask
        boundary_mask[boundary_mask > 0] = 1

        # Flood fill the contours to make them masks
        binary_mask = ModelCTCV1.flood_fill(boundary_mask)

        return binary_mask, boundary_mask

    @staticmethod
    def flood_fill(_x: np.ndarray):
        """
        :param _x: Image containing contours to be flood filled
        :return binary_mask: np.ndarray: Mask of filled given contours within _x

        Note: Helper function
        """
        # Shallow copy _x
        x_copy = np.copy(_x)
        boundary_mask_contour = x_copy.astype('uint8')

        # Initialize binary mask as zeroes
        w = _x.shape[0] + 2
        h = _x.shape[1] + 2
        binary_mask = np.zeros((w, h), dtype='uint8')

        # OpenCV Flood Fill implementation
        cv2.floodFill(boundary_mask_contour, binary_mask, (0, 0), 255)

        # Take the inversion to get the mask
        binary_mask = cv2.bitwise_not(binary_mask)
        binary_mask = binary_mask - 254

        # Remove additional pixels on the edge that were added necessarily for OpenCV implementation
        binary_mask = binary_mask[1:-1, 1:-1]

        return binary_mask

    @staticmethod
    def maximum_cost_path_based_lung_separation(_x: np.ndarray, mask: np.ndarray):
        """
        Step 5/6 in the classical model

        :param _x: Rotationally aligned CT slice
        :param mask: Binary mask of lung pixels
        :return (x_copy, lungs_extracted_binary): (np.ndarray, np.ndarray):  tuple of _x with pixels outside of the lung
            boundaries set to 0, as well as the corresponding binary mask.

        Note: Reference section 2.2.2 of https://www.hindawi.com/journals/cmmm/2016/2962047/
              Reference http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.18.7030&rep=rep1&type=pdf

            Erodes the image until 2 contours are found, dilates the image until just before only 1 contour is found
        """

        # Shallow copy _x, mask
        x_copy = np.copy(_x)
        mask_copy = np.copy(mask)

        # Select initial pixel values from lung mask for return matrix
        x_copy[mask_copy == 0] = 0

        # Select initial pixel values from lung mask for lung separation
        lungs_extracted = np.copy(_x)
        lungs_extracted[mask_copy == 0] = 0

        # Preprocess matrix for OpenCV - converting to positive UINT8
        lungs_extracted = lungs_extracted * -1
        lungs_extracted = (lungs_extracted / np.amax(_x)) * 255
        lungs_extracted = lungs_extracted.astype('uint8')

        # Extract lung contours
        lung_mask, number_connected_masks = ndimage.measurements.label(lungs_extracted)

        # If there is only one contour (possibility for both lungs to be connected)...
        if number_connected_masks == 1:

            # Hardcoded diamond structuring element for erosion and dilation
            diamond_structuring_element = np.array([[0, 0, 0, 1, 0, 0, 0],
                                                    [0, 0, 1, 1, 1, 0, 0],
                                                    [0, 1, 1, 1, 1, 1, 0],
                                                    [1, 1, 1, 1, 1, 1, 1],
                                                    [0, 1, 1, 1, 1, 1, 0],
                                                    [0, 0, 1, 1, 1, 0, 0],
                                                    [0, 0, 0, 1, 0, 0, 0]], dtype='uint8')

            # While the two lungs are still connected...
            while 1 <= number_connected_masks < 2:
                # Erode the lung mask
                lungs_extracted = cv2.erode(lungs_extracted, diamond_structuring_element)
                # Check the number of masks again
                lung_mask, number_connected_masks = ndimage.measurements.label(lungs_extracted)

            # While the lungs are still separated...
            while number_connected_masks > 1:

                # Limit any dilation to the original mask
                lungs_extracted[mask_copy == 0] = 0

                # Dilate the mask
                lungs_extracted_dilated = cv2.dilate(lungs_extracted, diamond_structuring_element, iterations=1)
                # Check the number of masks again
                lung_mask, number_connected_masks = ndimage.measurements.label(lungs_extracted_dilated)

                # If there is more than one mask, integrate dilation
                if number_connected_masks > 1:
                    lungs_extracted = lungs_extracted_dilated

            # Reduce the previously trimmed x_copy to the separated mask
            x_copy[lungs_extracted == 0] = 0

        # Binarize mask
        lungs_extracted[lungs_extracted > 0] = 1
        # Flood fill in case erosion/dilation caused pockets
        lungs_extracted_binary = ModelCTCV1.flood_fill(lungs_extracted)

        return x_copy, lungs_extracted_binary

    @staticmethod
    def median_border_smoothing(_x: np.ndarray):
        """
        Step 6/6 in the classical model

        :param _x: Binary mask to be smoothened
        :return: Smoothened binary mask

        Note: Median border smoothing was chosen over the cited Pulmonary Parenchyma Refinement in section 2.3 of
              https://www.hindawi.com/journals/cmmm/2016/2962047/ , for ease of implementation with apparent
              comparable accuracy.

        """
        # OpenCV Median Blur used to smoothen the border of masks instead of the cited Parenchyma Refinement
        return cv2.medianBlur(_x, ksize=5)

    @staticmethod
    def directional_pooling(_x: np.ndarray, threshold: int = 3):
        """
        :param _x: Numpy array containing a contour
        :param threshold: Threshold for directional pooling, minimum number of times the pooled information must overlap
            to create a mask.
        :return center_pooling: np.ndarray:  Binary mask (0 or 255) generated from the input contour

        Note: Helper function

            In some cases, contours generated are not completely enclosed, and would vanish if flood filled.
            This 'directional pooling' was created to find a reasonable approximation of the mask even without
            complete contour enclosure. This is a custom technique and was inspired by directional pooling found in
            CornerNet: https://arxiv.org/pdf/1808.01244.pdf and CenterNet: https://arxiv.org/pdf/1904.08189.pdf .
        """

        assert 0 < threshold < 5

        # Shallow copy _x
        x_copy = np.copy(_x)

        # Instantiate directional pooling arrays
        directional_up = np.zeros_like(x_copy)
        directional_down = np.zeros_like(x_copy)
        directional_left = np.zeros_like(x_copy)
        directional_right = np.zeros_like(x_copy)

        # Pool right
        for row_iter in range(x_copy.shape[0]):
            max_ = 0
            for column_iter in range(x_copy.shape[1]):
                max_ = max(max_, x_copy[row_iter, column_iter])
                directional_right[row_iter, column_iter] = max_/255

        # Pool left
        for row_iter in range(x_copy.shape[0]):
            max_ = 0
            for column_iter in reversed(range(x_copy.shape[1])):
                max_ = max(max_, x_copy[row_iter, column_iter])
                directional_left[row_iter, column_iter] = max_/255

        # Pool down
        for column_iter in range(x_copy.shape[1]):
            max_ = 0
            for row_iter in range(x_copy.shape[0]):
                max_ = max(max_, x_copy[row_iter, column_iter])
                directional_down[row_iter, column_iter] = max_/255

        # Pool up
        for column_iter in range(x_copy.shape[1]):
            max_ = 0
            for row_iter in reversed(range(x_copy.shape[0])):
                max_ = max(max_, x_copy[row_iter, column_iter])
                directional_up[row_iter, column_iter] = max_/255

        # Add all directional pools to aggregate them
        center_pooling = directional_up + directional_down + directional_left + directional_right

        # Use threshold to binarize aggregated pooling
        center_pooling[center_pooling < threshold] = 0
        center_pooling[center_pooling >= threshold] = 255

        return center_pooling

    @staticmethod
    def moore_neighborhood_boundary_tracing(starting_point: tuple, _x: np.ndarray,
                                            starting_direction: int):
        """
        :param starting_point: Tuple containing (x, y) coordinates of starting pixel to begin contour search
        :param _x: Binary mask with lower-value possible air pixels as '0' and higher value external pixels as '1'
        :param starting_direction: Initial direction to begin 8-neighborhood search, int [0, 7].
        :return: list(tuple(int, int)): List of coordinates traced during contour detection

        Note: Helper function, used in diagonal tracing based lung contouring.
            Reference: http://www.imageprocessingplace.com/downloads_V3/root_downloads/tutorials/
            contour_tracing_Abeer_George_Ghuneim/moore.html
            Implementation of Moor's Neighborhood boundary tracing (aka 8-neighborhood-based boundary tracing) with
            Jacob's stopping criterion.
            Finds a contour within a mask given a starting point for that contour.
        """
        assert 0 <= starting_direction < 8

        # Initialize vars
        visited_dict = {}
        current_direction = starting_direction
        current_point = starting_point

        # While any given point has not been visited from the same direction twice...
        while True:

            # Find the next point and direction
            next_point, next_direction = ModelCTCV1.moore_neighborhood_boundary_tracing_scan(current_point, _x,
                                                                                             current_direction)

            # If the point has not been visited before...
            if next_point not in visited_dict:
                # Remember the point and direction
                visited_dict[next_point] = {next_direction}
            # If the point has been visited but from a different direction...
            elif next_direction not in visited_dict[next_point]:
                # Add direction to point direction set
                visited_dict[next_point].add(next_direction)
            # Otherwise given point has been visited from the same direction - break
            else:
                break

            # Assign next direction and point
            current_direction = next_direction - 2
            current_point = next_point

        return [x for x in visited_dict.keys()]

    @staticmethod
    def moore_neighborhood_boundary_tracing_scan(point, _x, starting_direction, seek=0):
        """
        :param point: Tuple containing (x, y) coordinates of currently searching pixel
        :param _x: Binary mask with lower-value possible air pixels as '0' and higher value external pixels as '1'
        :param starting_direction: Direction in which to start local search for 'seek' value
        :param seek: Pixel value
        :return: (probed_x, probed_y), direction % 8: tuple(tuple(int, int), int): Coordinated of found next pixel
            within Moore's Neighborhood boundary tracing algorithm

        Note: Performs a single step within Moore's Neighborhood boundary tracing algorithm
        """
        # Hardcoded position and direction dictionary - going clockwise and using column, row
        positional_dictionary = OrderedDict([(0, (-1, -1)), (1, (0, -1)), (2, (1, -1)),
                                             (3, (1, 0)), (4, (1, 1)),
                                             (5, (0, 1)), (6, (-1, 1)), (7, (-1, 0))])

        # Scan clockwise for next point equalling 'seek' inside contour with a given direction
        for direction in range(starting_direction, starting_direction + 8):
            current_position = positional_dictionary[direction % 8]
            probed_x = point[0] + current_position[0]
            probed_y = point[1] + current_position[1]
            if _x[probed_y][probed_x] == seek:
                return (probed_x, probed_y), direction % 8

        return None
