from src.Pipeline import PipelineTCIA
from src.Task import TaskerTCIA
from src.Visualize import Visualizer

if __name__ == '__main__':

    dirs = ["./data_subset/LIDC/LIDC-IDRI/",
            "./data_subset/HNSCC/HNSCC/",
            "./data_subset/CPTAC/CPTAC-UCEC/"]

    data_path_list = "./src/data_paths_subset.csv"
    model_load_path = None  # "../Model Saves/V1.0 95.36% Val Acc/"
    model_save_path = "../model_saves/Most Recent Save/"
    visualizations_save_path = "../visualizations/"

    DIMENSIONS = (64, 64, 3)
    BATCH_SIZE = 16
    EPOCHS = 1

    pipe = PipelineTCIA(DIMENSIONS, BATCH_SIZE)
    pipe.load_from_study_dirs(dirs)

    model, history = TaskerTCIA.train(pipe, EPOCHS, model_load_path, model_save_path)

    ground_truths, predictions = TaskerTCIA.inference_over_test(model_save_path, pipe)

    Visualizer.visualize_training_history(history)
    Visualizer.visualize_metrics(ground_truths, predictions, view=False, save_dir=visualizations_save_path)

    pass


########################################################################################################################

# TODO: find more non-lung images in dataset,  move to cloud, experiment with different resolutions / feature planes,
#  model refining, create readme, create docstring on Evaluate and Visualize classes


########################################################################################################################
