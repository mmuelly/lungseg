from imblearn.under_sampling import RandomUnderSampler
import random
import pandas as pd
import math
import pydicom
import numpy as np
from src.Extract import ExtractorTCIA
from src.Extract import ExtractorLUNA
from src.Transform import TransformerTCIA
from src.Augment import Augmentor
import tensorflow as tf
import functools
import SimpleITK as sitk


class PipelineTCIA:

    def __init__(self, input_dimensions, batch_size, initially_shuffle_data: bool = False, balance_data: bool = False):

        self.input_dimensions = input_dimensions

        self.batch_size = batch_size
        assert self.batch_size > 0

        self.initially_shuffle_data = initially_shuffle_data
        self.balance_data = balance_data

        self.verbose_level = 0

        self.train_percent = 0.8
        self.test_percent = 0.2
        assert self.train_percent + self.test_percent <= 1

        self.slice_paths = []
        self.counts = None

        self.train_slice_paths = []
        self.test_slice_paths = []

        self.number_train_batches = None
        self.number_test_batches = None

    def extract(self, study_dirs: list):
        """
        :param study_dirs: List of top-level directories from The Cancer Imaging Archive
        :return None:

        Note: Transfers all DICOM paths within the given top-level directories into self.slice_paths using the
             ExtractorTCIA class.
        """
        for study_dir in study_dirs:
            self.slice_paths.extend(ExtractorTCIA(study_dir, self.verbose_level).slice_paths)

        return

    def load_from_study_dirs(self, study_dirs: list):
        """
        :param study_dirs: List of top-level directories from The Cancer Imaging Archive
        :return None:

        Note: Handles all facets of loading the data from a list of directories,
             including balancing and splitting the dataset pulled from TCIA directiories into test and train.
        """

        self.extract(study_dirs)

        slice_path_df = pd.DataFrame(self.slice_paths)
        self.counts = self.find_slice_category_counts(slice_path_df)

        if self.balance_data:
            self.balance_dataset()

        if self.initially_shuffle_data:
            random.shuffle(self.slice_paths)

        self.split_data()
        self.reduce_data_to_whole_batches()

        return

    def split_data(self):
        """
        :return None:

        Note: Splits the data represented by paths of CT slices into test and train. Test and train percentages are
             attributes of self.
        """

        self.train_slice_paths = self.slice_paths[0:math.floor(self.train_percent * len(self.slice_paths))]
        self.test_slice_paths = self.slice_paths[-(math.floor(self.test_percent * len(self.slice_paths))):]

        return

    @staticmethod
    def load_from_dicom(slice_path: str):
        """
        :param slice_path: Path to a DICOM file representing a CT slice
        :return dicom_array: np.ndarray: Numpy array containing the raw pixel data within the DICOM file
        """

        with pydicom.filereader.dcmread(slice_path) as current_dicom:
            dicom_array = current_dicom.pixel_array
            return dicom_array

    def store_slice_paths_as_csv(self, save_path: str):
        """
        :param save_path: The path, including the file and file extension, where the slice paths are to be saved
        :return None:

        Note: Slice paths are stored and loaded from a csv instead of being navigated to every time because it is
             expensive to open the file initially in order to see its class.
        """

        slice_path_dataframe = pd.DataFrame(self.slice_paths)
        slice_path_dataframe.to_csv(save_path, header=False, index=False)

        return

    def load_slice_paths_from_csv(self, load_path: str):
        """
        :param load_path: The path, including the file and file extension, where the slice paths have been saved as a
                         csv
        :return None:

        Note: Transfers all DICOM paths within the given csv load path into self.slice_paths.
             Also handles balancing and splitting the dataset pulled from TCIA directiories into test and train.
        """

        slice_path_dataframe = pd.read_csv(load_path, header=None)
        self.counts = self.find_slice_category_counts(slice_path_dataframe)
        self.slice_paths = slice_path_dataframe.values.tolist()
        if self.balance_data:
            self.balance_dataset()
        self.split_data()
        self.reduce_data_to_whole_batches()

        return

    def reduce_data_to_whole_batches(self):
        """
        :return None:

        Note: Reduces the train and test datasets so that they are equally divisible between batches
        """

        self.train_slice_paths = self.train_slice_paths[:self.batch_size * (len(self.train_slice_paths)
                                                                            // self.batch_size)]
        self.test_slice_paths = self.test_slice_paths[:self.batch_size * (len(self.test_slice_paths)
                                                                          // self.batch_size)]

        self.number_train_batches = len(self.train_slice_paths) // self.batch_size
        self.number_test_batches = len(self.test_slice_paths) // self.batch_size

        return

    @staticmethod
    def find_slice_category_counts(slice_path_dataframe: pd.DataFrame):
        """
        :param slice_path_dataframe: Dataframe loaded from a csv containing DICOM paths and their respective classes
        :return class_counts: pd.Series: The classes within the given dataframe and how frequently they occur. Used
                             for balancing the dataset.
        """
        class_counts = pd.value_counts(slice_path_dataframe.iloc[:, [1]].values.flatten())
        return class_counts

    def create_tf_dataset(self,  mode: str = 'Train'):

        dataset_generator = functools.partial(self.yield_batch, mode)

        h, w, c = self.input_dimensions

        output_shape = ([self.batch_size, h, w, c], self.batch_size)

        dataset = tf.data.Dataset.from_generator(dataset_generator, output_types=('float32', 'float32'),
                                                 output_shapes=output_shape)

        return dataset

    def yield_batch(self, mode: str = 'Train'):
        """
        :param mode: The mode which the generator is in. 'Train' creates a generator which yields batches from the
            train dataset, while 'Test' yields batches from the test dataset
        :return (batch_x, batch_y): tuple: Yields a tuple of the batched input to a model in NHWC as well as the
            corresponding target variables.
        """

        assert mode in ('Train', 'Test')

        data_pair_generator = self.yield_single_train_data_pair(mode)

        while True:

            batch_x = np.zeros(shape=(self.batch_size, self.input_dimensions[0], self.input_dimensions[1],
                                      self.input_dimensions[2]))

            batch_y = np.zeros(shape=self.batch_size)

            for iter_1 in range(0, self.batch_size):
                x, y = next(data_pair_generator)
                batch_x[iter_1, :, :, :] = np.copy(x)
                batch_y[iter_1] = np.copy(y)

            yield batch_x, batch_y

    def yield_single_train_data_pair(self, mode: str = 'Train'):
        """
        :param mode: The mode which the generator is in. 'Train' creates a generator which yields a single data point
            from the train dataset, while 'Test' yields a single data point from the test dataset
        :return (x, y): tuple: Yields a tuple of a single input to a model in HWC as well as the corresponding
            target variable.
        """

        while True:
            if mode == 'Train':
                random.shuffle(self.train_slice_paths)
            elif mode == 'Test':
                random.shuffle(self.test_slice_paths)

            if mode == 'Train':
                for pair in self.train_slice_paths:
                    x = self.load_from_dicom(pair[0])
                    y = pair[1]

                    x = TransformerTCIA.transform(x, self.input_dimensions)

                    augmentor = Augmentor()
                    x = augmentor.augment(x)

                    y = np.array(y)

                    yield x, y

            elif mode == 'Test':
                for pair in self.test_slice_paths:
                    x = self.load_from_dicom(pair[0])
                    y = pair[1]

                    x = TransformerTCIA.transform(x, self.input_dimensions)
                    y = np.array(y)

                    yield x, y

    def balance_dataset(self):
        """
        :return None:

        Note: Uses Random Undersampling to balance the dataset between lung and non-lung slices.
        """

        under_sampler = RandomUnderSampler(random_state=42)
        slice_paths = [x[0] for x in self.slice_paths]
        slice_classes = np.array([x[1] for x in self.slice_paths])

        dummy_indices = np.arange(0, slice_classes.size)
        dummy_indices = np.expand_dims(dummy_indices, 1)
        dummy_indices, slice_classes = under_sampler.fit_resample(dummy_indices, slice_classes)

        under_sampled_slice_paths = [slice_paths[x[0]] for x in dummy_indices]
        under_sampled_slice_classes = slice_classes.tolist()

        self.slice_paths = list(zip(under_sampled_slice_paths, under_sampled_slice_classes))

        slice_path_df = pd.DataFrame(self.slice_paths)
        self.counts = self.find_slice_category_counts(slice_path_df)

        if self.initially_shuffle_data:
            random.shuffle(self.slice_paths)

        return

    def load_from_study_dir_and_store(self, study_dirs: list, save_path: str):
        """
        :param study_dirs: List of top-level directories from The Cancer Imaging Archive
        :param save_path: The path, including the file and file extension, where the slice paths are to be saved
        :return None:
        """

        self.load_from_study_dirs(study_dirs)
        self.store_slice_paths_as_csv(save_path)

        return


class PipelineLUNA:

    def __init__(self, raw_dir, mask_dir, verbose_level):
        self.raw_dir = raw_dir
        self.mask_dir = mask_dir
        self.verbose_level = verbose_level
        self.extractor = ExtractorLUNA(raw_dir, mask_dir, verbose_level)

    def yield_single_train_data_pair(self):
        """
        :return (x_slice, y_slice, scan_iteration): tuple(np.ndarray, np.ndarray, int) tuple of a raw CT slice,
            its corresponding ground truth mask, and the iteration of its scan.
        """
        scan_iteration = 0

        for x_path, y_path in zip(self.extractor.x_paths, self.extractor.y_paths):
            x_scan = self.load_from_zraw_mhd(x_path)
            y_scan = self.load_from_zraw_mhd(y_path)

            for slice_index in range(x_scan.shape[0]):
                x_slice = x_scan[slice_index]
                y_slice = y_scan[slice_index]

                yield x_slice, y_slice, scan_iteration

            scan_iteration += 1

    @staticmethod
    def load_from_zraw_mhd(load_path: str):
        """
        :param load_path: load path containing the .mhd/.zraw file pair, without file extensions
        :return ct_scan: np.ndarray: 3D numpy volume contained within the zraw file
        """
        # Reads the image using SimpleITK
        itkimage = sitk.ReadImage(load_path + '.mhd')

        # Convert the image to a  numpy array first and then shuffle the dimensions to get axis in the order z,y,x
        ct_scan = sitk.GetArrayFromImage(itkimage)

        return ct_scan


