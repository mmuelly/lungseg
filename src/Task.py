from src.Pipeline import PipelineTCIA
from src.Model import ModelCNNV1
import tensorflow.keras as k


class TaskerTCIA:

    @staticmethod
    def train(pipe_: PipelineTCIA, epochs: int, model_load_path_: str = None, model_save_path_: str = None):
        """
        :param epochs: The number of times the model is iteratively trained on the data
        :param pipe_: PipelineTCIA object which handles moving the data into the model
        :param model_save_path_: The folder which will contain the model checkpoint saves
        :param model_load_path_: The folder which contains the savepoint that will initialize the model if applicable
        :return (model, history): (keras.engine.training.Model, keras.callbacks.History):
        """

        if model_load_path_ is None:
            model = ModelCNNV1.return_model(input_dimensions=pipe_.input_dimensions)
        else:
            model = k.models.load_model(model_load_path_)

        model_checkpoint = k.callbacks.ModelCheckpoint(filepath=model_save_path_, save_best_only=True,
                                                       save_weights_only=False, mode='max')

        history = model.fit(x=pipe_.create_tf_dataset('Train'),
                            steps_per_epoch=pipe_.number_train_batches,
                            epochs=epochs,
                            validation_data=pipe_.create_tf_dataset('Test'),
                            validation_steps=pipe_.number_test_batches,
                            callbacks=[model_checkpoint])

        return model, history

    @staticmethod
    def inference_over_test(model_load_path_: str, pipe_: PipelineTCIA):

        model_ = k.models.load_model(model_load_path_)

        validation_ground_truth = []
        validation_predictions = []

        test_generator = pipe_.yield_batch(mode='Test')

        for batch_iteration in range(pipe_.number_test_batches):

            next_batch = next(test_generator)

            validation_ground_truth.extend(next_batch[1])
            validation_predictions.extend(model_(next_batch, training=False))

        return validation_ground_truth, validation_predictions
