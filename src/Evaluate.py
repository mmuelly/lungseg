from sklearn.metrics import roc_curve, confusion_matrix, precision_recall_curve, accuracy_score, auc


class Evaluator:

    @staticmethod
    def generate_receiver_operating_characteristic_curve(ground_truths: list, predictions: list):
        """
        :param ground_truths: A list of the ground truths corresponding to the input that the 
                              model has previously been predicted upon.
        :param predictions: A list of the predictions that the model has generated from a list of
                            inputs. Must match the ground truths in length and respective original 
                            image.
        :return false_positive_rate: X-values of an ROC curve, the number of false positives generated
                                     for a specific model sensitivity.
        :return true_positive_rate: X-values of an ROC curve, the number of true positives generated
                                     for a specific model sensitivity.
        :return thresholds: Decreasing thresholds on the decision function used to compute false positive
                            rate and true positive rate.
        :return area_under_the_curve: Amount of area under the ROC curve.
        """
        
        false_positive_rate, true_positive_rate, thresholds = roc_curve(ground_truths, predictions)
        area_under_the_curve = auc(false_positive_rate, true_positive_rate)

        return false_positive_rate, true_positive_rate, thresholds, area_under_the_curve

    @staticmethod
    def generate_precision_recall_curve(ground_truths: list, predictions: list):
        """
        :param ground_truths: A list of the ground truths corresponding to the input that the 
                              model has previously been predicted upon.
        :param predictions: A list of the predictions that the model has generated from a list of
                            inputs. Must match the ground truths in length and respective original 
                            image.
        :return precision: Precision values such that element i is the precision of predictions 
                           with score >= thresholds[i] and the last element is 1.
        :return recall: Decreasing recall values such that element i is the recall of predictions 
                        with score >= thresholds[i] and the last element is 0.
        :return thresholds: Increasing thresholds on the decision function used to compute 
                            precision and recall.
        :return area_under_the_curve: Amount of area under the PR curve.
        """
        
        precision, recall, thresholds = precision_recall_curve(ground_truths, predictions)
        area_under_the_curve = auc(recall, precision)

        return precision, recall, thresholds, area_under_the_curve

    @staticmethod
    def generate_confusion_matrix(ground_truths: list, predictions: list):
        """
        :param ground_truths: A list of the ground truths corresponding to the input that the 
                              model has previously been predicted upon.
        :param predictions: A list of the predictions that the model has generated from a list of
                            inputs. Must match the ground truths in length and respective original 
                            image.
        :return confusion_matrix_: Confusion matrix whose i-th row and j-th column entry indicates 
                                   the number of samples with true label being i-th class and prediced 
                                   label being j-th class.
        """
        
        prediction_classes = [0 if x < 0.5 else 1 for x in predictions]

        confusion_matrix_ = confusion_matrix(ground_truths, prediction_classes)

        return confusion_matrix_

    @staticmethod
    def generate_accuracy(ground_truths: list, predictions: list):
        """
        :param ground_truths: A list of the ground truths corresponding to the input that the 
                              model has previously been predicted upon.
        :param predictions: A list of the predictions that the model has generated from a list of
                            inputs. Must match the ground truths in length and respective original 
                            image.
        :return accuracy_score_: Percentage of correctly predicted images over total predictions.
        """
        
        prediction_classes = [0 if x < 0.5 else 1 for x in predictions]

        accuracy_score_ = accuracy_score(ground_truths, prediction_classes)

        return accuracy_score_
