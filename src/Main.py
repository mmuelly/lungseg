from src.Pipeline import PipelineTCIA
from src.Task import TaskerTCIA
from src.Visualize import Visualizer


def main_tcia():
    dirs = ["../data_subset/LIDC/LIDC-IDRI/",
            "../data_subset/HNSCC/HNSCC/",
            "../data_subset/CPTAC/CPTAC-UCEC/"]

    data_path_list = "./src/data_paths_subset.csv"
    model_load_path = None  # "../Model Saves/V1.0 95.36% Val Acc/"
    model_save_path = "../model_saves/Most Recent Save/"
    visualizations_save_path = "../visualizations/"

    dimensions = (64, 64, 3)
    batch_size = 16
    epochs = 1

    pipe = PipelineTCIA(dimensions, batch_size)
    pipe.load_from_study_dirs(dirs)

    model, history = TaskerTCIA.train(pipe, epochs, model_load_path, model_save_path)

    ground_truths, predictions = TaskerTCIA.inference_over_test(model_save_path, pipe)

    Visualizer.visualize_training_history(history)
    Visualizer.visualize_metrics(ground_truths, predictions, view=False, save_dir=visualizations_save_path)


def main_luna():
    from src.Pipeline import PipelineLUNA
    from src.Model import ModelCTCV1
    import numpy as np

    dir_ = '../data_subset/LUNA/'  # '../data_subset/test/'
    raw_dir = dir_ + 'raw_data/'
    mask_dir = dir_ + 'segmentation_masks/'
    pipe = PipelineLUNA(raw_dir, mask_dir, 0)
    data_generator = pipe.yield_single_train_data_pair()

    mctv1 = ModelCTCV1()

    data_block = []
    current_scan_iter = 0

    iter_1 = -1

    for x, y, scan_iter in data_generator:

        iter_1 += 1

        print("Slice " + str(iter_1))

        if current_scan_iter != scan_iter:

            Visualizer.visualize_volume(data_block)

            current_scan_iter = scan_iter
            data_block = []

        data_block.append(mctv1(x))


if __name__ == '__main__':

    main_luna()
