import numpy as np
import random
from tensorflow.keras.preprocessing.image import ImageDataGenerator


class Augmentor:

    def __init__(self):

        self.salt_and_pepper_rate = 0.1

        # Goal data generator parameters
        '''
        self.tf_idg_params = {'featurewise_center': False,
                              'samplewise_center': False,
                              'featurewise_std_normalization': False,
                              'samplewise_std_normalization': False,
                              'zca_whitening': False,
                              'zca_epsilon': 1e-06,
                              'rotation_range': 10,
                              'width_shift_range': 0.05,
                              'height_shift_range': 0.05,
                              'brightness_range': (0.95, 1.05),
                              'shear_range': 5,
                              'zoom_range': (0.95, 1.05),
                              'channel_shift_range': 0.0,
                              'fill_mode': 'constant',
                              'cval': 0.0,
                              'horizontal_flip': True,
                              'vertical_flip': False,
                              'rescale': None,
                              'preprocessing_function': None,
                              'data_format': 'channels_last',
                              'validation_split': 0.0,
                              'dtype': np.float32}
        '''

        # Currently testing data generator parameters
        self.tf_idg_params = {'featurewise_center': False,
                              'samplewise_center': False,
                              'featurewise_std_normalization': False,
                              'samplewise_std_normalization': False,
                              'zca_whitening': False,
                              'zca_epsilon': 1e-06,
                              'rotation_range': 10,
                              'width_shift_range': 0,
                              'height_shift_range': 0,
                              'brightness_range': None,
                              'shear_range': 0,
                              'zoom_range': 0,
                              'channel_shift_range': 0.0,
                              'fill_mode': 'constant',
                              'cval': 0.0,
                              'horizontal_flip': False,
                              'vertical_flip': False,
                              'rescale': None,
                              'preprocessing_function': None,
                              'data_format': 'channels_last',
                              'validation_split': 0.0,
                              'dtype': np.float32}

        return

    def augment(self, numpy_array: np.ndarray):
        """
        :param numpy_array: Numpy array to be augmented
        :return augmented_array: np.ndarray: Augmented numpy array

        Note: represents the entire process of augmentation
        """

        salt_and_pepper_array = self.augment_salt_and_pepper(numpy_array)
        augmented_array = self.augment_tf(salt_and_pepper_array)

        return augmented_array

    def augment_salt_and_pepper(self, numpy_array: np.ndarray):
        """
        :param numpy_array: Numpy array to be augmented using salt and pepper data augmentation
        :return salt_and_pepper_array: np.ndarray: Numpy array after salt and pepper data augmentation is applied
        """

        salt_and_pepper_array = np.zeros_like(numpy_array)

        for channel in range(numpy_array.shape[2]):
            for row in range(numpy_array.shape[0]):
                for column in range(numpy_array.shape[1]):

                    next_random_number = random.random()
                    if next_random_number < self.salt_and_pepper_rate:
                        if next_random_number < self.salt_and_pepper_rate / 2:
                            salt_and_pepper_array[row, column, channel] = 0
                        else:
                            salt_and_pepper_array[row, column, channel] = 1
                    else:
                        salt_and_pepper_array[row, column, channel] = numpy_array[row, column, channel]

        return salt_and_pepper_array

    def augment_tf(self, numpy_array: np.ndarray):
        """
        :param numpy_array: Numpy array to be augmented using tf.keras.preprocessing.image.ImageDataGenerator
        :return augmented_array: np.ndarray: Numpy array after being augmented by the ImageDataGenerator
        """

        image_data_generator = ImageDataGenerator(featurewise_center=self.tf_idg_params['featurewise_center'],
                                                  samplewise_center=self.tf_idg_params['samplewise_center'],
                                                  featurewise_std_normalization=self.tf_idg_params
                                                  ['featurewise_std_normalization'],
                                                  samplewise_std_normalization=self.tf_idg_params
                                                  ['samplewise_std_normalization'],
                                                  zca_whitening=self.tf_idg_params['zca_whitening'],
                                                  zca_epsilon=self.tf_idg_params['zca_epsilon'],
                                                  rotation_range=self.tf_idg_params['rotation_range'],
                                                  width_shift_range=self.tf_idg_params['width_shift_range'],
                                                  height_shift_range=self.tf_idg_params['height_shift_range'],
                                                  brightness_range=self.tf_idg_params['brightness_range'],
                                                  shear_range=self.tf_idg_params['shear_range'],
                                                  zoom_range=self.tf_idg_params['zoom_range'],
                                                  channel_shift_range=self.tf_idg_params['channel_shift_range'],
                                                  fill_mode=self.tf_idg_params['fill_mode'],
                                                  cval=self.tf_idg_params['cval'],
                                                  horizontal_flip=self.tf_idg_params['horizontal_flip'],
                                                  vertical_flip=self.tf_idg_params['vertical_flip'],
                                                  rescale=self.tf_idg_params['rescale'],
                                                  preprocessing_function=self.tf_idg_params['preprocessing_function'],
                                                  data_format=self.tf_idg_params['data_format'],
                                                  validation_split=self.tf_idg_params['validation_split'],
                                                  dtype=self.tf_idg_params['dtype'])

        augmented_array = image_data_generator.random_transform(numpy_array)

        return augmented_array
