import os
import pydicom


class ExtractorTCIA:
    def __init__(self, dir_: str, verbose_level: int):
        self.verbose_level = verbose_level
        self.number_lung_slices = 0
        self.number_not_lung_slices = 0

        self.dir = dir_
        self.subject_dirs, self.subjects = self.get_subjects(dir_)

        self.slice_paths = self.get_categorized_slice_paths()

    @staticmethod
    def get_subjects(dir_: str):
        """
        :param dir_: Directory which contains folders representing The Cancer Imaging Archive subjects
        :return (subject_dirs: list, subjects: list): Tuple containing lists of the directories of the subjects
            and their respective ids
        """

        sub_dirs = os.listdir(dir_)
        subject_dirs = [os.path.join(dir_, x) for x in sub_dirs if os.path.isdir(os.path.join(dir_, x))]
        subjects = [x for x in sub_dirs if os.path.isdir(os.path.join(dir_, x))]

        return subject_dirs, subjects

    def get_categorized_slice_paths(self):
        """
        :return slice_paths: list: List of all DICOM directories within self.subject_dirs and each
            DICOM's repective class

        Note: The Cancer Imaging Archive data is organized in a hierarchal format, with the following folders
            going from top to bottom: Subjects -> Hospital Visits -> Radoilogical Recording/Scan -> DICOM files.
            DICOM files are filtered and classed by the filter_dicom method.

        """

        if self.subject_dirs is None:
            return

        slice_paths = []

        for subject_dir in self.subject_dirs:
            if self.verbose_level > 0:
                print(subject_dir)
            visits = os.listdir(subject_dir)
            for visit in visits:
                visit_dir = os.path.join(subject_dir + '/', visit)
                recordings = os.listdir(visit_dir)
                for recording in recordings:
                    if self.verbose_level > 1:
                        print('\t' + recording)
                    recording_dir = os.path.join(visit_dir + '/', recording)
                    slices = [x for x in os.listdir(recording_dir) if x.endswith('.dcm')]

                    # Check data in first slice to see if the recording is worth keeping
                    first_slice_path = os.path.join(recording_dir + '/', slices[0])
                    slice_category = self.filter_dicom(first_slice_path)

                    if slice_category == 'Lungs':
                        if self.verbose_level > 1:
                            print("\t\tLungs")
                    elif slice_category == 'Not Lungs':
                        if self.verbose_level > 1:
                            print("\t\tNot Lungs")
                    elif slice_category == 'Excl':
                        if self.verbose_level > 1:
                            print("\t\tExcluded")
                        continue

                    for slice_ in slices:
                        slice_path = os.path.join(recording_dir + '/', slice_)
                        if slice_category == 'Lungs':
                            self.number_lung_slices += 1
                            slice_paths.append([slice_path, 1])
                        elif slice_category == 'Not Lungs':
                            self.number_not_lung_slices += 1
                            slice_paths.append([slice_path, 0])

        return slice_paths

    @staticmethod
    def filter_dicom(slice_path: str):
        """
        :param slice_path: Path to a single DICOM file
        :return slice_type: str: Class of the slice - 'Lungs', 'Not Lungs', and 'Excl'. 'Excl' slices are not included.
        """
        with pydicom.filereader.dcmread(slice_path) as current_dicom:

            slice_type = "Excl"

            if hasattr(current_dicom, 'SeriesDescription'):
                if current_dicom.SeriesDescription in ('CT Atten Cor H&N', 'MiednicaPOZNE  5.0  B31f'):
                    slice_type = 'Not Lungs'
            if hasattr(current_dicom, 'PatientID'):
                if current_dicom.PatientID.startswith('LIDC'):
                    slice_type = 'Lungs'
            if hasattr(current_dicom, 'Modality'):
                if current_dicom.Modality != 'CT':
                    slice_type = 'Excl'

            return slice_type


class ExtractorLUNA:

    def __init__(self, x_dir: str, y_dir: str, verbose_level: int):
        self.x_dir = x_dir
        self.y_dir = y_dir
        self.verbose_level = verbose_level
        self.sort_paths = True
        self.x_paths, self.y_paths = self.get_data_pairs(x_dir, y_dir)

    def get_data_pairs(self, x_dir: str, y_dir: str):
        """
        :param x_dir: String representing the directory containing the raw CT scans in .mhd/.zraw form
        :param y_dir: String representing the directory containing the ground truth CT Masks in .mhd/.zraw form
        :return tuple(list(str), list(str)): Tuple of x, y pair columns
        """
        x_files = os.listdir(x_dir)
        y_files = os.listdir(y_dir)

        x_files = ExtractorLUNA.verify_zraw_mhd_pairs(x_files)
        y_files = ExtractorLUNA.verify_zraw_mhd_pairs(y_files)

        x_y_intersection = ExtractorLUNA.verify_x_y_pairs(x_files, y_files)
        if self.sort_paths:
            x_y_intersection = list((sorted(x_y_intersection)))

        x_files = [x_dir + x for x in x_y_intersection]
        y_files = [y_dir + x for x in x_y_intersection]

        return x_files, y_files

    @staticmethod
    def verify_zraw_mhd_pairs(file_list: list):
        """
        :param file_list: list of files within a given directory
        :return list(second_file_occurance): list(str): list of verified files with .mhd/.zraw pairs in directory

        Note: Ensures existence of both .mhd and .zraw file pairs in given directory
        """
        first_file_occurance = set()
        second_file_occurance = set()
        rejected_files = set()

        for file_string in file_list:
            file_key = file_string.split('.')[:-1]
            file_key = [x + '.' for x in file_key]
            file_key = ''.join(file_key)
            file_key = file_key[:-1]
            if file_key not in first_file_occurance:
                first_file_occurance.add(file_key)
            elif file_key not in second_file_occurance:
                second_file_occurance.add(file_key)
            elif file_key not in rejected_files:
                rejected_files.add(file_key)

        for rejected_file in rejected_files:
            second_file_occurance.remove(rejected_file)

        return list(second_file_occurance)

    @staticmethod
    def verify_x_y_pairs(file_list_1: list, file_list_2: list):
        """
        :param file_list_1: list of verified files with .mhd/.zraw pairs in x directory
        :param file_list_2: list of verified files with .mhd/.zraw pairs in y directory
        :return:

        Note: Ensures existence of both x and y pairs in given directories
        """
        file_list_1_set = set(file_list_1)
        file_list_2_set = set(file_list_2)

        return list(file_list_1_set.intersection(file_list_2_set))

