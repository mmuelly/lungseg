FROM tensorflow/tensorflow:latest-gpu 

RUN apt-get update
RUN apt-get install -y libsm6 libxext6 libxrender-dev

RUN mkdir -p /usr/deployment
WORKDIR /usr/deployment/

RUN python -m pip install --upgrade pip

COPY ./requirements.txt /usr/deployment/
RUN pip install -r requirements.txt --ignore-installed

RUN mkdir -p /usr/deployment/src
COPY ./src/ /usr/deployment/src/

RUN mkdir -p /usr/deployment/test
COPY ./test/ /usr/deployment/test/

RUN mkdir -p /usr/deployment/model_saves
COPY ./model_saves/ /usr/deployment/model_saves/

RUN mkdir -p /usr/deployment/model_saves
COPY ./model_saves/ /usr/deployment/model_saves/

ENV PYTHONPATH "$PYTHONPATH:/usr/deployment"

CMD ["python", "./src/Main.py"]